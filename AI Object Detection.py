# AI Object Detection with Jetson Nano
# Using OpenCV and jetson inference to detect and identify objects
# Mike Soniat
# 2022

import jetson_inference
import jetson_utils
import time
import cv2 
import numpy as np 

timeStamp = time.time()
fpsFiltered = 0

net = jetson_inference.detectNet('ssd-mobilenet-v2', threshold = .5)
dispW = 1208
dispH = 720
flip = 0
font = cv2.FONT_HERSHEY_SIMPLEX

# pi cam
# cam = jetson_utils.gstCamera(dispW, dispH, '0')
camSet='nvarguscamerasrc wbmode=2 tnr-mode=2 tnr-strength=1 ee-mode=2 ee-strength=1 !  video/x-raw(memory:NVMM), width=3264, height=2464, '
camSet = camSet + 'format=NV12, framerate=21/1 ! nvvidconv flip-method='+str(flip)+' ! video/x-raw, width='+str(dispW)+', height='
camSet = camSet + str(dispH)+', format=BGRx ! videoconvert ! video/x-raw, format=BGR ! videobalance contrast=1.5 brightness=-.1 '
camSet = camSet + 'saturation=1.2 ! appsink drop=true' 
cam=cv2.VideoCapture(camSet)

# web cam
# cam = jetson_utils.gstCamera(dispW, dispH, '/dev/video1')
# display = jetson_utils.glDisplay()
# cam = cv2.VideoCapture('/dev/video1')
# cam.set(cv2.CAP_PROP_FRAME_WIDTH, dispW)
# cam.set(cv2.CAP_PROP_FRAME_HEIGHT, dispH)

while True:
    # img, width, height = cam.CaptureRGBA()
    _, img = cam.read()
    width = img.shape[1]
    height = img.shape[0]

    # convert color for net.Detect
    frame = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA).astype(np.float32)

    # convert to cuda array
    frame = jetson_utils.cudaFromNumpy(frame)

    detections = net.Detect(frame, width, height)

    for detect in detections:
        ID = detect.ClassID
        item = net.GetClassDesc(ID)
        top = detect.Top
        left = detect.Left
        bottom = detect.Bottom
        right = detect.Right

        cv2.rectangle(img,(int(left),int(top)),(int(right),int(bottom)),(0,255,0), 2)
        cv2.putText(img, item, (int(left), int(top)+20), font, 1, (0,0,255), 2)

    dt = time.time() - timeStamp
    timeStamp = time.time()

    fps = 1/dt
    fpsFiltered = .9 * fpsFiltered + .1 * fps

    cv2.putText(img, str(round(fpsFiltered,1)) + ' fps', (0,30), font, 1, (0,0,255),2)
    cv2.imshow('detCam', img)
    cv2.moveWindow('detCam', 0,0)

    if cv2.waitKey(1)==ord('q'):
        break
cam.release()
cv2.destroyAllWindows()